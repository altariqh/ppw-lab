from django.conf.urls import url
from .views import index, add_friend, validate_npm, delete_friend, friend_list, data_friend_list, details_friend

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^add-friend/$', add_friend, name='add-friend'),
	url(r'^validate-npm/$', validate_npm, name='validate-npm'),
	url(r'^get-friend-list/delete1/(?P<friend_id>[0-9]+)/$', delete_friend, name='delete-friend'),
	url(r'^get-friend-list/$', friend_list, name='get-friend-list'),
	url(r'^get-friend-list/details/(?P<friend_id>[0-9]+)/$', details_friend, name='details'),
    url(r'^data-friend-list/$', data_friend_list, name='data-friend-list'),
]